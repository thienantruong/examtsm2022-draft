import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os

df = pd.read_csv('../Data/part1.csv')


# Plotting the distribution of the cost in each course

df.plot.hist(figsize =(10, 8),title = "Distribution of the cost for each course")
plt.xlabel("Costs")
plt.show()

# Plotting the distribution of the cost in each course seperatly
fig, axes = plt.subplots(1,3)
df["FIRST_COURSE"].plot.hist(title = "Distribution of the cost for first course", figsize = (20, 7), color = "tab:blue", xlabel ="Costs", ax = axes[0])
df["SECOND_COURSE"].plot.hist(title = "Distribution of the cost for second course", figsize = (20, 7), color = "#ff8000", xlabel ="Costs", ax = axes[1])
df["THIRD_COURSE"].plot.hist(title = "Distribution of the cost for third course", figsize = (20, 7), color = "g", xlabel ="Costs", ax = axes[2])
plt.show()

# Barplot of the cost per course

    # Calculating the average cost per course
Firstcostaverage = df["FIRST_COURSE"].mean()
Secondcostaverage = df["SECOND_COURSE"].mean()
Thirdcostaverage = df["THIRD_COURSE"].mean()
    #Plotting
Frequency = [Firstcostaverage, Secondcostaverage, Thirdcostaverage]
Courses = ["FIRST_COST", "SECOND_COST", "THIRD_COST"]
fig, ax = plt.subplots(figsize = (20, 5))
ax.set(title ="Barplot of the cost per course", xlabel = "Average cost", ylabel = "Course")
plt.barh(Courses, Frequency, color = ['tab:blue', 'tab:orange', 'tab:green'], align = "center")
plt.show()

# Create additional columns for food and drink
class Interval:
    def __init__(self, start, end):
        assert start <= end
        self.start = start
        self.end = end

    def __contains__(self, number):  # this dunder method lets you use 'in'
        return self.start <= number < self.end

#FIRST_COURSE

s_soup = Interval(3,15)
s_tomato_moza = Interval(15,20)
s_oysters = Interval(20,25)

First_course_food = []
First_course_food_name = []
First_course_drink = []

for i in range(0,len(df)):
    if df.iloc[i,2] in s_soup:
        First_course_food.append(3)
        First_course_food_name.append("Soup")
        First_course_drink.append(df.iloc[i, 2] - 3)
    elif df.iloc[i,2] in s_tomato_moza:
        First_course_food.append(15)
        First_course_food_name.append("Tomato Mozarella")
        First_course_drink.append(df.iloc[i,2]-15)
    elif df.iloc[i,2] in s_oysters:
        First_course_food.append(20)
        First_course_food_name.append("Oysters")
        First_course_drink.append(df.iloc[i, 2] - 20)
    else:
        First_course_food.append(0)
        First_course_food_name.append("-")
        First_course_drink.append(0)

df['1ST_C_FOOD'] = First_course_food
df['1ST_C_FOOD_NAME'] = First_course_food_name
df['1ST_C_DRINK'] = First_course_drink

#SECOND_COURSE

s_salad = Interval(9,20)
s_spaghetti = Interval(20,25)
s_steak = Interval(25,40)
s_lobsters = Interval(40,45)

Second_course_food = []
Second_course_food_name = []
Second_course_drink = []

for i in range(0,len(df)):
    if df.iloc[i,3] in s_salad:
        Second_course_food.append(9)
        Second_course_food_name.append("Salad")
        Second_course_drink.append(df.iloc[i, 3] - 9)
    elif df.iloc[i,3] in s_spaghetti:
        Second_course_food.append(20)
        Second_course_food_name.append("Spaghetti")
        Second_course_drink.append(df.iloc[i,3]-20)
    elif df.iloc[i,3] in s_steak:
        Second_course_food.append(25)
        Second_course_food_name.append("Steak")
        Second_course_drink.append(df.iloc[i, 3] - 25)
    elif df.iloc[i,3] in s_lobsters:
        Second_course_food.append(40)
        Second_course_food_name.append("Lobsters")
        Second_course_drink.append(df.iloc[i, 3] - 40)
    else:
        Second_course_food.append(0)
        Second_course_food_name.append("-")
        Second_course_drink.append(0)

df['2ND_C_FOOD'] = Second_course_food
df['2ND_C_FOOD_NAME'] = Second_course_food_name
df['2ND_C_DRINK'] = Second_course_drink

#THIRD_COURSE

s_pie = Interval(10,15)
s_icecream = Interval(15,20)

Third_course_food = []
Third_course_food_name = []
Third_course_drink = []

for i in range(0,len(df)):
    if df.iloc[i,4] in s_pie:
        Third_course_food.append(10)
        Third_course_food_name.append("Pie")
        Third_course_drink.append(df.iloc[i, 4] - 10)
    elif df.iloc[i,4] in s_icecream:
        Third_course_food.append(15)
        Third_course_food_name.append("Ice Cream")
        Third_course_drink.append(df.iloc[i,4]-15)
    else:
        Third_course_food.append(0)
        Third_course_food_name.append("-")
        Third_course_drink.append(0)

df['3RD_C_FOOD'] = Third_course_food
df['3RD_C_FOOD_NAME'] = Third_course_food_name
df['3RD_C_DRINK'] = Third_course_drink

print(df)

import seaborn as sns
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from sklearn.preprocessing import MinMaxScaler
from mpl_toolkits.mplot3d import Axes3D

df = pd.read_csv('../Data/part1.csv')
np.random.seed(5) #why using this?  

x = df[["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE"]].values



kmeans = KMeans(n_clusters = 4, init = 'k-means++', max_iter = 300, n_init = 10, random_state = 0)
y_kmeans = kmeans.fit_predict(x)

plt.scatter(x[y_kmeans == 0, 0], x[y_kmeans == 0, 1], c = 'purple', label = 'Iris-setosa')
plt.scatter(x[y_kmeans == 1, 0], x[y_kmeans == 1, 1], c = 'orange', label = 'Iris-versicolour')
plt.scatter(x[y_kmeans == 2, 0], x[y_kmeans == 2, 1], c = 'green', label = 'Iris-virginica')
plt.scatter(x[y_kmeans == 3, 0], x[y_kmeans == 3, 1], c = 'blue', label = 'Iris-smth')

#Plotting the centroids of the clusters
plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:,1], c = 'red', label = 'Centroids')

plt.legend()

# 3d scatterplot using matplotlib

fig = plt.figure(figsize = (10,10))
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x[y_kmeans == 0, 0], x[y_kmeans == 0, 1], x[y_kmeans == 0, 2], c = 'purple', label = 'Iris-setosa')
ax.scatter(x[y_kmeans == 1, 0], x[y_kmeans == 1, 1], x[y_kmeans == 1,2], c = 'orange', label = 'Iris-versicolour')
ax.scatter(x[y_kmeans == 2, 0], x[y_kmeans == 2, 1], x[y_kmeans == 2, 2], c = 'green', label = 'Iris-virginica')
ax.scatter(x[y_kmeans == 3, 0], x[y_kmeans == 3, 1], x[y_kmeans == 3, 2], c = 'blue', label = 'Iris-smth')
#Plotting the centroids of the clusters
ax.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:,1], s = 50, c = 'red', label = 'Centroids')
plt.show()